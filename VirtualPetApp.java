import java.util.Scanner;
public class VirtualPetApp{
	
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Dog[] dogPack = new Dog[4];
		
		for(int i = 0; i < dogPack.length; i++){
			
			//asks for dogBreed input 
			System.out.println("What is your favourite dog breed?");
			String dogBreed = reader.nextLine();
			
			//asks for dogSize input
			System.out.println("What size of dog would you prefer: small, average, large?");
			String dogSize = reader.nextLine();
			
			//asks for energylvl input
			System.out.println("What level of energy are you seeking in a dog: energetic, non-energetic, moderately energetic?");
			String energyLvl = reader.nextLine();
			
			//create Dog object with inputs in order to store it into the array
			dogPack[i] = new Dog(dogBreed, dogSize, energyLvl);
		
		}
		System.out.println("Final dog breed of choice: " + dogPack[3].dogBreed);
		System.out.println("Final dog size of choice: " + dogPack[3].dogSize);
		System.out.println("Energy level prefered: " + dogPack[3].energyLvl);
		
		System.out.println(dogPack[0].printDog());
		System.out.println(dogPack[0].printDogSize());
	}
}