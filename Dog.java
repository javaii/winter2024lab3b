public class Dog{
	public String dogBreed;
	public String dogSize;
	public String energyLvl;
	
	public Dog(String dogBreed, String dogSize, String energyLvl){
		this.dogBreed = dogBreed;
		this.dogSize = dogSize;
		this.energyLvl = energyLvl;
	}	
	
	public String printDog(){
		return ("My favourite type of dog is a " + dogBreed + " which would have to be a " + energyLvl + " dog.");
	}
	
	public String printDogSize(){
		return ("To fit my lifestyle, I would want a " + dogSize + " size dog.");
		
	}
}
	